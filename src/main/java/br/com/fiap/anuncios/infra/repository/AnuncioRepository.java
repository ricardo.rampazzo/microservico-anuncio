package br.com.fiap.anuncios.infra.repository;

import br.com.fiap.anuncios.core.model.AnuncioModel;
import br.com.fiap.anuncios.core.model.ListaAnuncioModel;

public interface AnuncioRepository {

    AnuncioModel salvarAnuncio(AnuncioModel anuncioModel);

    ListaAnuncioModel consultarAnuncioByCodigo(Integer codigoAnuncio);

    ListaAnuncioModel consultarAnuncios();

    AnuncioModel alterarAnuncio(AnuncioModel anuncioModel);

    void deletarAnuncio(Integer codigoAnuncio);
}
