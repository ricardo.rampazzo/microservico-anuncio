package br.com.fiap.anuncios.infra.repository.crud;

import br.com.fiap.anuncios.infra.entity.AnuncioEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AnuncioCrudRepository extends CrudRepository<AnuncioEntity, Integer> {

    List<AnuncioEntity> findAll();

    List<AnuncioEntity> findByCodigoAnuncio(Integer codigoAnuncio);

}
