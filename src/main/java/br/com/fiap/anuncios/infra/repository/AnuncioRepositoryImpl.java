package br.com.fiap.anuncios.infra.repository;

import br.com.fiap.anuncios.core.model.AnuncioModel;
import br.com.fiap.anuncios.core.model.ListaAnuncioModel;
import br.com.fiap.anuncios.infra.converter.AnuncioConverter;
import br.com.fiap.anuncios.infra.entity.AnuncioEntity;
import br.com.fiap.anuncios.infra.repository.crud.AnuncioCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class AnuncioRepositoryImpl implements AnuncioRepository {

    @Autowired
    AnuncioConverter anuncioConverter;

    @Autowired
    AnuncioCrudRepository anuncioCrudRepository;

    public AnuncioModel salvarAnuncio(AnuncioModel anuncioModel) {
        AnuncioEntity anuncioEntity = anuncioConverter.convertModelToEntity(anuncioModel);

        return anuncioConverter.convertEntityToModel(anuncioCrudRepository.save(anuncioEntity));
    }

    public ListaAnuncioModel consultarAnuncios() {

        List<AnuncioModel> listaAnuncios = new ArrayList<>();

        List<AnuncioEntity> listaAnunciossEntity = anuncioCrudRepository.findAll();

        listaAnunciossEntity.forEach(item -> {
            AnuncioModel anuncioModel = anuncioConverter.convertEntityToModel(item);
            listaAnuncios.add(anuncioModel);
        });

        ListaAnuncioModel listaAnuncioModel = new ListaAnuncioModel(listaAnuncios);

        return listaAnuncioModel;
    }

    public ListaAnuncioModel consultarAnuncioByCodigo(Integer codigoAnuncio) {

        List<AnuncioModel> listaAnuncios = new ArrayList<>();

        List<AnuncioEntity> listaAnunciossEntity = anuncioCrudRepository.findByCodigoAnuncio(codigoAnuncio);

        listaAnunciossEntity.forEach(item -> {
            AnuncioModel anuncioModel = anuncioConverter.convertEntityToModel(item);
            listaAnuncios.add(anuncioModel);
        });

        ListaAnuncioModel listaAnuncioModel = new ListaAnuncioModel(listaAnuncios);

        return listaAnuncioModel;
    }

    public AnuncioModel alterarAnuncio(AnuncioModel anuncioModel) {
        Optional<AnuncioEntity> anuncioById = anuncioCrudRepository.findById(anuncioModel.getCodigoAnuncio());
        AnuncioModel anuncio = new AnuncioModel();

        if (anuncioById.isPresent()) {
            AnuncioEntity anuncioEntity = anuncioConverter.convertModelToEntity(anuncioModel);

            anuncio = anuncioConverter.convertEntityToModel(anuncioCrudRepository.save(anuncioEntity));
        } else {
            throw new RuntimeException("Página não encontrada");
        }
        return anuncio;
    }

    public void deletarAnuncio(Integer codigoAnuncio) {

        anuncioCrudRepository.deleteById(codigoAnuncio);
    }

}
