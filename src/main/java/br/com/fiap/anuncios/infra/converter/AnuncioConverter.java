package br.com.fiap.anuncios.infra.converter;

import br.com.fiap.anuncios.core.model.AnuncioModel;
import br.com.fiap.anuncios.infra.entity.AnuncioEntity;
import org.springframework.stereotype.Component;

@Component
public class AnuncioConverter {

    public AnuncioEntity convertModelToEntity(AnuncioModel anuncioModel) {
        AnuncioEntity anuncioEntity = new AnuncioEntity();
        anuncioEntity.setCodigoAnuncio(anuncioModel.getCodigoAnuncio());
        anuncioEntity.setTituloAnuncio(anuncioModel.getTituloAnuncio());
        anuncioEntity.setResumoAnuncio(anuncioModel.getResumoAnuncio());
        anuncioEntity.setTextoAnuncio(anuncioModel.getTextoAnuncio());
        anuncioEntity.setNomeUsuario(anuncioModel.getNomeUsuario());
        anuncioEntity.setEmailUsuario(anuncioModel.getEmailUsuario());
        anuncioEntity.setContatoWhatsApp(anuncioModel.getContatoWhatsApp());
        anuncioEntity.setHoraAula(anuncioModel.getHoraAula());
        anuncioEntity.setIndicadorAulaOnline(anuncioModel.getIndicadorAulaOnline());
        anuncioEntity.setIndicadorAulaPresencial(anuncioModel.getIndicadorAulaPresencial());
        anuncioEntity.setCodigoCategoria(anuncioModel.getCodigoCategoria());
        anuncioEntity.setDataCriacao(anuncioModel.getDataCriacao());
        anuncioEntity.setDataAlteracao(anuncioModel.getDataAlteracao());

        return anuncioEntity;
    }

    public AnuncioModel convertEntityToModel(AnuncioEntity anuncioEntity) {
        AnuncioModel anuncioModel = new AnuncioModel();
        anuncioModel.setCodigoAnuncio(anuncioEntity.getCodigoAnuncio());
        anuncioModel.setTituloAnuncio(anuncioEntity.getTituloAnuncio());
        anuncioModel.setResumoAnuncio(anuncioEntity.getResumoAnuncio());
        anuncioModel.setTextoAnuncio(anuncioEntity.getTextoAnuncio());
        anuncioModel.setNomeUsuario(anuncioEntity.getNomeUsuario());
        anuncioModel.setEmailUsuario(anuncioEntity.getEmailUsuario());
        anuncioModel.setContatoWhatsApp(anuncioEntity.getContatoWhatsApp());
        anuncioModel.setHoraAula(anuncioEntity.getHoraAula());
        anuncioModel.setIndicadorAulaOnline(anuncioEntity.getIndicadorAulaOnline());
        anuncioModel.setIndicadorAulaPresencial(anuncioEntity.getIndicadorAulaPresencial());
        anuncioModel.setCodigoCategoria(anuncioEntity.getCodigoCategoria());
        anuncioModel.setDataCriacao(anuncioEntity.getDataCriacao());
        anuncioModel.setDataAlteracao(anuncioEntity.getDataAlteracao());

        return anuncioModel;
    }

}
