package br.com.fiap.anuncios.infra.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
@Table(name = "anuncios")
@NoArgsConstructor
@AllArgsConstructor
public class AnuncioEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "anuncios")
    @Column(name = "CODIGO_ANUNCIO")
    private Integer codigoAnuncio;

    @Column(name = "TITULO_ANUNCIO", length = 26)
    @NotNull
    private String tituloAnuncio;

    @Column(name = "RESUMO_ANUNCIO", length = 100)
    @NotNull
    private String resumoAnuncio;

    @Column(name = "TEXTO_ANUNCIO", length = 1000)
    @NotNull
    private String textoAnuncio;

    @Column(name = "NOME_USUARIO", length = 100)
    @NotNull
    private String nomeUsuario;

    @Column(name = "EMAIL_USUARIO", length = 100)
    @NotNull
    private String emailUsuario;

    @Column(name = "CONTATO", length = 11)
    @NotNull
    private String contatoWhatsApp;

    @Column(name = "HORA_AULA")
    @NotNull
    private int horaAula;

    @Column(name = "AULA_ONLINE", length = 1)
    @NotNull
    private String indicadorAulaOnline;

    @Column(name = "AULA_PRESENCIAL", length = 1)
    @NotNull
    private String indicadorAulaPresencial;

    @Column(name = "CODIGO_CATEGORIA")
    @NotNull
    private Integer codigoCategoria;

    @Column(name = "DATA_CRIACAO")
    private Date dataCriacao;

    @Column(name = "DATA_ALTERACAO")
    private Date dataAlteracao;
}
