package br.com.fiap.anuncios.core.ports.out;

import br.com.fiap.anuncios.core.model.AnuncioModel;
import br.com.fiap.anuncios.core.model.ListaAnuncioModel;
import br.com.fiap.anuncios.infra.repository.AnuncioRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class AnuncioPortImpl implements AnuncioPort {

    @Autowired
    private AnuncioRepository anuncioRepository;

    @Override
    public AnuncioModel cadastrarAnuncio(AnuncioModel anuncioModel) {

        return anuncioRepository.salvarAnuncio(anuncioModel);
    }

    @Override
    public ListaAnuncioModel consultarAnuncios() {

        return anuncioRepository.consultarAnuncios();
    }

    @Override
    public ListaAnuncioModel consultarAnuncioByCodigo(Integer codigoAnuncio) {

        return anuncioRepository.consultarAnuncioByCodigo(codigoAnuncio);
    }

    @Override
    public AnuncioModel alterarAnuncio(AnuncioModel anuncioModel) {

        return anuncioRepository.alterarAnuncio(anuncioModel);
    }

    @Override
    public void deletarAnuncio(Integer codigoAnuncio) {

        anuncioRepository.deletarAnuncio(codigoAnuncio);
    }

}
