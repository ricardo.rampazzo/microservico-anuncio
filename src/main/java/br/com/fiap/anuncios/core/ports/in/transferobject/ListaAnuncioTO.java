package br.com.fiap.anuncios.core.ports.in.transferobject;

import br.com.fiap.anuncios.core.model.AnuncioModel;
import br.com.fiap.anuncios.core.model.ListaAnuncioModel;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ListaAnuncioTO {

    @JsonProperty("anuncios")
    private List<AnuncioTO> listaAnuncio;

    public ListaAnuncioTO(ListaAnuncioModel listaAnuncioModel) {
        this.listaAnuncio = new ArrayList<>();

        for (AnuncioModel anuncioModel : listaAnuncioModel.getListaAnuncio()) {
            AnuncioTO anuncioTO = new AnuncioTO(anuncioModel);
            listaAnuncio.add(anuncioTO);
        }
    }
}
