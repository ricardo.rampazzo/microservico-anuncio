package br.com.fiap.anuncios.core.ports.in;

import br.com.fiap.anuncios.core.model.AnuncioModel;
import br.com.fiap.anuncios.core.model.ListaAnuncioModel;
import br.com.fiap.anuncios.core.ports.in.transferobject.AnuncioTO;
import br.com.fiap.anuncios.core.ports.in.transferobject.ListaAnuncioTO;
import br.com.fiap.anuncios.core.usecase.AnuncioUseCase;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;

@Log4j2
@RestController
@RequestMapping("/anuncios")
public class AnuncioControllerImpl implements AnuncioController {

    @Autowired
    AnuncioUseCase anuncioUseCase;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AnuncioTO cadastrarAnuncio(@RequestBody @Valid AnuncioTO anuncioTO) {
        AnuncioModel anuncioModel = new AnuncioModel(anuncioTO);

        AnuncioTO categoria = new AnuncioTO(anuncioUseCase.cadastrarAnuncio(anuncioModel));

        return categoria;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ListaAnuncioTO consultarAnuncios() {

        ListaAnuncioModel listaAnuncioModel = anuncioUseCase.consultarAnuncios();

        ListaAnuncioTO listaAnuncioTO = new ListaAnuncioTO(listaAnuncioModel);

        return listaAnuncioTO;
    }

    @GetMapping("/{codigoAnuncio}")
    @ResponseStatus(HttpStatus.OK)
    public ListaAnuncioTO consultarAnuncioByCodigo(@PathVariable Integer codigoAnuncio) {

        ListaAnuncioModel listaAnuncioModel = anuncioUseCase.consultarAnuncioByCodigo(codigoAnuncio);

        ListaAnuncioTO listaAnuncioTO = new ListaAnuncioTO(listaAnuncioModel);

        return listaAnuncioTO;
    }

    @PatchMapping
    @ResponseStatus(HttpStatus.OK)
    public AnuncioTO alterarAnuncio(@RequestBody @Valid AnuncioTO anuncioTO) {
        AnuncioModel anuncioModel = new AnuncioModel(anuncioTO);

        AnuncioTO anuncio = new AnuncioTO(anuncioUseCase.alterarAnuncio(anuncioModel));

        return anuncio;
    }

    @DeleteMapping("/{codigoAnuncio}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deletarAnuncio(@PathVariable Integer codigoAnuncio) {

        anuncioUseCase.deletarAnuncio(codigoAnuncio);

    }

}
