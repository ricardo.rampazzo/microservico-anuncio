package br.com.fiap.anuncios.core.ports.in.transferobject;

import br.com.fiap.anuncios.core.model.AnuncioModel;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class AnuncioTO {

    @JsonProperty("codigo_anuncio")
    private int codigoAnuncio;

    @JsonProperty("titulo_anuncio")
    private String tituloAnuncio;

    @JsonProperty("resumo_anuncio")
    private String resumoAnuncio;

    @JsonProperty("texto_anuncio")
    private String textoAnuncio;

    @JsonProperty("contato")
    private String contatoWhatsApp;

    @JsonProperty("hora_aula")
    private int horaAula;

    @JsonProperty("aula_online")
    private String indicadorAulaOnline;

    @JsonProperty("aula_presencial")
    private String indicadorAulaPresencial;

    @JsonProperty("nome_usuario")
    private String nomeUsuario;

    @JsonProperty("email_usuario")
    private String emailUsuario;

    @JsonProperty("codigo_categoria")
    private Integer codigoCategoria;

    @JsonProperty("data_criacao")
    private Date dataCriacao;

    @JsonProperty("data_alteracao")
    private Date dataAlteracao;

    public AnuncioTO(AnuncioModel anuncioModel) {
        this.codigoAnuncio = anuncioModel.getCodigoAnuncio();
        this.tituloAnuncio = anuncioModel.getTituloAnuncio();
        this.resumoAnuncio = anuncioModel.getResumoAnuncio();
        this.textoAnuncio = anuncioModel.getTextoAnuncio();
        this.contatoWhatsApp = anuncioModel.getContatoWhatsApp();
        this.horaAula = anuncioModel.getHoraAula();
        this.indicadorAulaOnline = anuncioModel.getIndicadorAulaOnline();
        this.indicadorAulaPresencial = anuncioModel.getIndicadorAulaPresencial();
        this.nomeUsuario = anuncioModel.getNomeUsuario();
        this.emailUsuario = anuncioModel.getEmailUsuario();
        this.codigoCategoria = anuncioModel.getCodigoCategoria();
        this.dataCriacao = anuncioModel.getDataCriacao();
        this.dataAlteracao = anuncioModel.getDataAlteracao();
    }

}
