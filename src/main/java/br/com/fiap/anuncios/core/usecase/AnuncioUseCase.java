package br.com.fiap.anuncios.core.usecase;

import br.com.fiap.anuncios.core.model.AnuncioModel;
import br.com.fiap.anuncios.core.model.ListaAnuncioModel;
import org.springframework.stereotype.Component;

@Component
public interface AnuncioUseCase {

    AnuncioModel cadastrarAnuncio(AnuncioModel anuncioModel);

    ListaAnuncioModel consultarAnuncios();

    ListaAnuncioModel consultarAnuncioByCodigo(Integer codigoAnuncio);

    AnuncioModel alterarAnuncio(AnuncioModel anuncioModel);

    void deletarAnuncio(Integer codigoAnuncio);

}


