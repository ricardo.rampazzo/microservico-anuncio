package br.com.fiap.anuncios.core.usecase;

import br.com.fiap.anuncios.core.model.AnuncioModel;
import br.com.fiap.anuncios.core.model.ListaAnuncioModel;
import br.com.fiap.anuncios.core.ports.out.AnuncioPort;
import br.com.fiap.anuncios.infra.converter.AnuncioConverter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Log4j2
@Service
public class AnuncioUseCaseImpl implements AnuncioUseCase {

    @Autowired
    private AnuncioPort anuncioPort;

    @Autowired
    private AnuncioConverter anuncioConverter;


    @Override
    public AnuncioModel cadastrarAnuncio(AnuncioModel anuncioModel) {

        anuncioModel.setDataCriacao(new Date());

        return anuncioPort.cadastrarAnuncio(anuncioModel);
    }

    @Override
    public ListaAnuncioModel consultarAnuncios() {

        ListaAnuncioModel listaAnuncioModel = anuncioPort.consultarAnuncios();

        return listaAnuncioModel;
    }

    @Override
    public ListaAnuncioModel consultarAnuncioByCodigo(Integer codigoAnuncio) {

        ListaAnuncioModel listaAnuncioModel = anuncioPort.consultarAnuncioByCodigo(codigoAnuncio);

        return listaAnuncioModel;
    }

    @Override
    public AnuncioModel alterarAnuncio(AnuncioModel anuncioModel) {

        anuncioModel.setDataAlteracao(new Date());

        return anuncioPort.alterarAnuncio(anuncioModel);
    }

    @Override
    public void deletarAnuncio(Integer codigoAnuncio) {

        anuncioPort.deletarAnuncio(codigoAnuncio);
    }

}
