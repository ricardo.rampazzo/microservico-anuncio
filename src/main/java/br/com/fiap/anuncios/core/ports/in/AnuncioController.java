package br.com.fiap.anuncios.core.ports.in;

import br.com.fiap.anuncios.core.ports.in.transferobject.ListaAnuncioTO;
import br.com.fiap.anuncios.core.ports.in.transferobject.AnuncioTO;
import org.springframework.stereotype.Component;

@Component
public interface AnuncioController {

    AnuncioTO cadastrarAnuncio(AnuncioTO anuncioTO);

    ListaAnuncioTO consultarAnuncios();

    ListaAnuncioTO consultarAnuncioByCodigo(Integer codigoAnuncio);

    AnuncioTO alterarAnuncio(AnuncioTO anuncioTO);

    void deletarAnuncio( Integer codigoAnuncio);

}
