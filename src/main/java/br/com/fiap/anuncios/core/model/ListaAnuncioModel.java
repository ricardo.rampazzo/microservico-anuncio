package br.com.fiap.anuncios.core.model;

import br.com.fiap.anuncios.core.ports.in.transferobject.AnuncioTO;
import br.com.fiap.anuncios.core.ports.in.transferobject.ListaAnuncioTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ListaAnuncioModel {

    private List<AnuncioModel> listaAnuncio;

    public ListaAnuncioModel(ListaAnuncioTO listaAnuncioTO) {
        this.listaAnuncio = new ArrayList<>();

        for (AnuncioTO anuncioTO : listaAnuncioTO.getListaAnuncio()) {
            AnuncioModel anuncioModel = new AnuncioModel(anuncioTO);
            listaAnuncio.add(anuncioModel);
        }
    }

}
