package br.com.fiap.anuncios.core.model;

import br.com.fiap.anuncios.core.ports.in.transferobject.AnuncioTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class AnuncioModel {

    private int codigoAnuncio;
    private String tituloAnuncio;
    private String resumoAnuncio;
    private String textoAnuncio;
    private String contatoWhatsApp;
    private int horaAula;
    private String indicadorAulaOnline;
    private String indicadorAulaPresencial;
    private String nomeUsuario;
    private String emailUsuario;
    private Integer codigoCategoria;
    private Date dataCriacao;
    private Date dataAlteracao;

    public AnuncioModel(AnuncioTO anuncioTO) {
        this.codigoAnuncio = anuncioTO.getCodigoAnuncio();
        this.tituloAnuncio = anuncioTO.getTituloAnuncio();
        this.resumoAnuncio = anuncioTO.getResumoAnuncio();
        this.textoAnuncio = anuncioTO.getTextoAnuncio();
        this.contatoWhatsApp = anuncioTO.getContatoWhatsApp();
        this.horaAula = anuncioTO.getHoraAula();
        this.indicadorAulaOnline = anuncioTO.getIndicadorAulaOnline();
        this.indicadorAulaPresencial = anuncioTO.getIndicadorAulaPresencial();
        this.nomeUsuario = anuncioTO.getNomeUsuario();
        this.emailUsuario = anuncioTO.getEmailUsuario();
        this.codigoCategoria = anuncioTO.getCodigoCategoria();
        this.dataCriacao = anuncioTO.getDataCriacao();
        this.dataAlteracao = anuncioTO.getDataAlteracao();
    }

}
