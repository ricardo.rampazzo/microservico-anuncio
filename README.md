# profnetapp

Projeto profnet baseado no protótipo https://www.figma.com/file/xOvOj4RWqwIEPTl9BcMzl0/Segunda-Etapa?type=design&node-id=0-1&t=54Fx6Haf93ApuxqV-0
Profnet é um aplicativo exclusivamente para pessoas que procuram professores particulares.

Nessa representação, o aplicativo tem uma tela de login integrada ao firebase com um usuário já criado, uma tela que lista todos os anúncios cadastrados e uma tela que o usuário pode criar um novo anúncio. O back-end é um microserviço criado nas fases anteriores.

Para rodar o projeto:

1 - Criar as tabelas referente ao back-end
	https://gitlab.com/ricardo.rampazzo/microservico-anuncios/-/tree/main/Scritps%20SQL

2 - Rodar o microserviço de anúncios
	https://gitlab.com/ricardo.rampazzo/microservico-anuncios
	
3 - Rodar o aplicativo profnet, trocando para seu IP local no nomento de criar a instância do retrofit
	https://gitlab.com/ricardo.rampazzo/profnetapp/-/blob/main/app/src/main/java/com/fiap/profnet/AnunciosActivity.kt linha 40
	https://gitlab.com/ricardo.rampazzo/profnetapp/-/blob/main/app/src/main/java/com/fiap/profnet/CriarAnuncioActivity.kt linha 54
	
4 - Para fazer login:
	Usuário: fase2@fiap.com.br
	Senha: fase2123

# Link para o trello:
https://trello.com/b/iB2mVIrE/planejamento-fase2-fiap-sub